<? require "bd.php"?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Тестовое задание РП Технологии</title>
	<!-- bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!-- end bootstrap -->
	<link rel="stylesheet" href="css/main.css">
</head>
<body>

	<div class="container">
		<div class="row align-items-center justify-content-between">
			<h1>
				Список сотрудников
			</h1>
			<!-- <button class="workers-list__add" title="Добавить">add</button> -->
			<button class="button" data-toggle="modal" data-target="#addnew"  title="Добавить нового сотрудника">Добавить сотрудника</button>
		</div>

		<div class="row">
			<table class="workers-list">
				<thead class="workers-list__header">
					<tr>
						<th class="workers-list__title workers-list__title--name">
							ФИО сотрудника
						</th>
						<th class="workers-list__title workers-list__title--data">
							Дата рождения
						</th>
						<th class="workers-list__title workers-list__title--gender">
							Пол
						</th>
						<th class="workers-list__title workers-list__title--btns">
							
						</th>
					</tr>
				</thead>
				<tbody>
					
					<!-- id и data-worker из БД -->
		<?
						$list_workers = $mysqli->query("SELECT * FROM workers");
						$post = array();
						while($row = mysqli_fetch_assoc($list_workers)){
							$posts[] = $row;
						}

						if (isset($posts)){
							foreach ($posts as $post){
								?>
								<tr id="worker<?echo($post["id"])?>" data-worker="1" class="workers-list__one worker">
									<td class="worker__info worker__info--name"><?echo($post["name"]);?></td>
									<td class="worker__info worker__info--data"><?echo($post["datebd"]);?></td>
									<td class="worker__info worker__info--gender">
										<?
										if($post["gender"] == 0){echo("Мужской");}else{echo("Женский");}
										?>
									</td>
									<td class="worker__info worker__info--btns">
										<button type="button" data-toggle="modal" data-target="#workermodal<?echo($post["id"])?>" class="button worker__btn worker__btn--view"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 576 512"><path fill="currentColor" d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"/></svg></button>
										<button class="button worker__btn worker__btn--delete" data-toggle="modal" data-target="#workerdel" data-worker="<?echo($post["id"])?>"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"/></svg></button>
									</td>
									<article class="modal fade" id="workermodal<?echo($post["id"])?>" tabindex="-1" role="dialog" aria-labelledby="workermodal<?echo($post["id"])?>__title" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<header class="modal-header">
													<h5 class="modal-title" id="workermodal<?echo($post["id"])?>__title">Просмотр информации</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"/></svg>
													</button>
												</header>
												<div class="modal-body view-info">
													<!-- Имя из БД -->
													<input hidden disabled type="number" class="view-info__val--target" value="<?echo($post["id"])?>">
													<p class="view-info__char">Имя: <input disabled class="view-info__val view-info__val--name" value="<?echo($post["name"]);?>"></p>
													<p class="view-info__char">Дата рождения: <input disabled class="view-info__val view-info__val--date" type="date" value="<?echo($post["datebd"]);?>"></p>
													<p class="view-info__char">Пол: 
														<select disabled class="view-info__val view-info__val--gender" value="<?echo($post["gender"])?>">
														<?php if (!$post["gender"]){
															?><option value="0" selected>Мужской</option><option value="1">Женский</option><?
														} else {
															?><option value="0">Мужской</option><option value="1" selected>Женский</option>
														<?}?>
															
														</select>
													</p>
													<!-- место работы -->
													<header class="view-info__header d-flex align-items-center justify-content-between">
														<h5>Предыдущие места работы:</h5>
														<button class="button new-job-add">Добавить место работы</button>
													</header>
													<div class="view-info__job">
														<p class="view-info__attr view-info__attr--name">Название компании</p>
														<p class="view-info__attr view-info__attr--data">Дата начала</p>
														<p class="view-info__attr view-info__attr--data">Дата окончания</p>
														<p class="view-info__attr view-info__attr--btns">Действие</p>
													</div>
													<!-- используется div, вместо table из-за проблему вложености таблиц -->
													<!-- в id хранится ключ для изменения информации в БД -->
												<?	
													if (!$list_jobs = $mysqli->query("SELECT * FROM jobs WHERE worker_id='".$post["id"]."'")) {
													    echo 'Ошибка: '.$mysqli->errno .' '. $mysqli->error;
													}
												    $jobs = array();
												    while($row = mysqli_fetch_assoc($list_jobs))
												    {
												        $jobs[] = $row;
												    }
												    if (isset($jobs)){
														foreach ($jobs as $jobs_one){
															
												?>
													<div class="view-info__job" id="<? echo($jobs_one["id"])?>">
														<!-- job-target хранится id места работы -->
														<input class="view-info--target" type="text" disabled name="job-target" value="<? echo($jobs_one["id"])?>" hidden form="edit-job">


														<input type="text" class="view-info__attr view-info__attr--name view-info__attr--company" value="<? echo($jobs_one["name"])?>" disabled form="edit-job">
														<input type="date" class="view-info__attr view-info__attr--data view-info--data-start" value="<? echo($jobs_one["datestart"])?>" disabled form="edit-job">
														<input type="date" class="view-info__attr view-info__attr--data view-info--data-end" value="<? echo($jobs_one["dateend"])?>" disabled form="edit-job">
														<p class="view-info__attr view-info__attr--btns">
															<button class="button edit-job-btn view-info__btn"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 576 512"><path fill="currentColor" d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"/></svg></button>
															<button class="button close-job-btn view-info__btn--close">
																<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"/></svg>
															</button>
															<button class="button save-job-btn view-info__btn view-info__btn--save" type="submit">
																<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z"/></svg>
															</button>

														</p>
													</div>
														<?
														}
													}
												?>


													<div class="form-job new-job">
														<h2 class="form-job__title">Добавить новое место работы</h2>
														<!-- Подставляется значение id работника -->
														<div class="form-job__inputs">	
															<input hidden type="number" class="form-job--new-target" value="<?echo($post["id"])?>">
															<div for="job-name" class="form-job__label">
																<p class="form-job__input-title">Название компании</p>
																<input class="form-job__input form-job--new-name" type="text" placeholder="ООО Компания">
															</div>
															<div for="job-start" class="form-job__label">
																<p class="form-job__input-title">Дата начала работы</p>
																<input class="form-job__input form-job--new-data-start" type="date">
															</div>
															<div for="job-end" class="form-job__label">
																<p class="form-job__input-title">Дата завершения работы</p>
																<input class="form-job__input form-job--new-data-end" type="date">
															</div>
															<div class="">
																<button class="button form-job__send" type="submit"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z"/></svg></button>
																<button class="button new-job-close" type="submit"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg></button>
															</div>
														</div>
													</div>						      	
												</div>
												<div class="modal-footer cahnge-worker">
													<button class="button change-worker--save"  title="Сохранить изменения о сотруднике">
														<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z"></path></svg>
													</button>
													<button class="button change-worker--close"  title="Закрыть без изменений">
														<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg>
													</button>
													<button type="button" class="button change-worker--edit" title="Изменить запись о сотруднике"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 576 512"><path fill="currentColor" d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"/></svg></button>

													<button type="button" class="button worker__btn--delete" title="Удалить запись о сотруднике"  data-toggle="modal" data-target="#workerdel" data-worker="<?echo($post["id"])?>"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"/></svg></button>
												</div>
											</div>
										</div>
									</article>


								<?
							}
						}

					?>
				
					
				</tbody>
				
			</table>		
		</div>
	</div>
	<div class="modal fade" id="workerdel" tabindex="-1" role="dialog" aria-labelledby="workerdel__title" aria-hidden="true">
		<div class="modal-dialog modal-window" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="workerdel1__title">Удалить данного сотрудника?</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"/></svg>
					</button>
				</div>
				<div class="modal-body">
					<!-- id сотрудника для удаления -->
					<input hidden type="text" name="delete-target" form="remove-worker">
					<div class="modal-window__btns">
						<button class="button" form="remove-worker">Да</button>
						<button class="button" data-dismiss="modal" aria-label="Close">Нет</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="addnew" tabindex="-1" role="dialog" aria-labelledby="workermodal1__title" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<form action="create-worker.php" method="post" class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="workermodal1__title">Добавить сотрудника</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"/></svg>
					</button>
				</div>
				<div class="modal-body view-info">
					<!-- Имя из БД -->
					<p class="view-info__char">Имя: <input  class="view-info__val" name="create-name" placeholder="Иванов Иван"></p>
					<p class="view-info__char">Дата рождения: <input name="create-bd" class="view-info__val" type="date"></p>
					<p class="view-info__char">Пол: <select class="view-info__val" name="create-gender"><option value="0">Мужской</option><option value="1">Женский</option></select></p>				      	
				</div>
				<div class="modal-footer cahnge-worker">
					<button type="submit" class="button"  title="Сохранить изменения о сотруднике">
						<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 448 512"><path fill="currentColor" d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z"></path></svg>
					</button>


					<button type="button" class="button" title="Закрыть без сохранения" data-dismiss="modal" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false" class="btn-icon" role="img" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg></button>
				</div>
			</form>
		</div>
	</div>
</div>	

<form hidden action="/new-job.php" method="post" id="new-job">
	<input type="text"name="new-job-name">
	<input type="date" name="new-job-start" >
	<input type="date" name="new-job-end" >
	<input type="number" name="new-job-target">
</form>

<form hidden action="/edit-job.php" method="post" id="edit-job">
	<input type="text"name="edit-job-name">
	<input type="date" name="edit-job-start" >
	<input type="date" name="edit-job-end" >
	<input type="number" name="edit-job-target">
</form>

<form hidden action="/edit-worker.php" method="post" id="edit-worker">
	<input type="text" name="edit-worker-name">
	<input type="date" name="edit-worker-bd" >
	<input type="number" name="edit-worker-gender">
	<input type="number" name="edit-worker-target">
</form>
<form hidden action="/remove-worker.php" method="post" id="remove-worker"></form>

<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<!-- end jquery -->
<!-- bootstrap -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!-- end bootstrap -->
<script src="js/main.js"></script>

</body>
</html>