<?
// начало работы, соединение с БД и создается нужные таблицы в БД
global $mysqli;
$mysqli = new mysqli('localhost', 'root', '', 'rp');
if ($mysqli->connect_errno) {
    echo "<h2 style='color: red; text-transform: uppercase; font-family: sans-serif;'>Ошибка соединения с БД: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error . "</h2>";
}
// создание таблиц для работы
$users_table = $mysqli->query("CREATE TABLE workers (
	id int NOT NULL AUTO_INCREMENT,
 	name varchar(80),
 	datebd date,
 	gender bit,
 	PRIMARY KEY (id)
);");

$codes_table = $mysqli->query("CREATE TABLE jobs (
	id int NOT NULL AUTO_INCREMENT,
	worker_id int,
	datestart date,
	dateend date,
 	name varchar(80),
 	PRIMARY KEY (id)
);");
