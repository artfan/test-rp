// открытие формы добавить рабочее место
$(".new-job-add").on("click", function(){
	$(".new-job").toggleClass("form-job--show");
});
// закрытие формы добавить рабочее место
$(".new-job-close").on("click", function(){
	$(".new-job").toggleClass("form-job--show");
});

$(".edit-job-btn").on("click", function(){
	// нахождение всех импутов и отображение кнопки Сохранить
	form = $(this).parents(".view-info__job");
	form.find("input").removeAttr("disabled");
	form.find(".view-info__btn--save").css("display","inline-block");
	// Настройка кнопки Изменить на Закрыть
	$(this).css("display", "none");
	form.find(".close-job-btn").css("display","inline-block");
});

// обработчик нажатия кнопки закрыть форму без редактирования
$(".close-job-btn").on("click", function(){
	form = $(this).parents(".view-info__job");
	$(this).css("display", "none");
	form.find(".view-info__btn--save").css("display","none");
	form.find(".edit-job-btn").css("display","inline-block");
	form.find("input").prop("disabled", "true");
});

// редактирование данных учетной записи работника
$(".change-worker--edit").on("click", function(){
	// нахождение всех импутов и отображение кнопки Сохранить
	form = $(this).parents(".modal");
	form.find(".view-info__val").removeAttr("disabled");
	// Настройка кнопки Изменить на Закрыть
	form.find(".change-worker--save").css("display","inline-block");
	form.find(".change-worker--close").css("display","inline-block");
	$(this).css("display", "none");
});
// обработчик нажатия кнопки закрыть форму без редактирования
$(".change-worker--close").on("click", function(){
	form = $(this).parents(".modal");
	$(this).css("display", "none");
	form.find(".change-worker--save").css("display","none");
	form.find(".change-worker--edit").css("display","inline-block");
	form.find("input").prop("disabled", "true");
});


$(".view-info__btn--save").on("click", function(){
	form = $(this).parents(".view-info__job");
	console.log(form.find(".view-info__btn--save").val());
});

$(".close-form").on("click", function(){
	$(this).parent().removeClass("form-job--show");
});
// запрос подтверждения на удаление 
// берем из data-worker id записи в бд и присваиваем его input'у который передаст информацию в php
$(".worker__btn--delete").on("click", function(){
	$(".delete-target").val($(this).parents(".worker").attr("data-worker"));
	console.log($(".delete-target").val($(this).parents(".worker").attr("data-worker")));
});

// перенос значений из инпутов в модальном окне в форму для редактирования места работы
// редактирование места работы
$(".view-info__btn--save").on("click", function(){
	form = $(this).parents(".view-info__job");
	$("input[name=edit-job-name]").val(form.find($("input.view-info__attr--company")).val());
	$("input[name=edit-job-start]").val(form.find($("input.view-info--data-start")).val());
	$("input[name=edit-job-end]").val(form.find($("input.view-info--data-end")).val());
	$("input[name=edit-job-target]").val(form.find($("input.view-info--target")).val());
	console.log('send');
	$("#edit-job").submit();
});

// создание нового места работы
$(".form-job__send").on("click", function(){
	form = $(this).parents(".form-job__inputs");
	$("input[name=new-job-name]").val(form.find($("input.form-job--new-name")).val());
	$("input[name=new-job-start]").val(form.find($("input.form-job--new-data-start")).val());
	$("input[name=new-job-end]").val(form.find($("input.form-job--new-data-end")).val());
	$("input[name=new-job-target]").val(form.find($("input.form-job--new-target")).val());
	console.log('send2');
	$("#new-job").submit();
});

//редактирование нового места работы
$(".form-job__send").on("click", function(){
	form = $(this).parents(".form-job__inputs");
	$("input[name=new-job-name]").val(form.find($("input.form-job--new-name")).val());
	$("input[name=new-job-start]").val(form.find($("input.form-job--new-data-start")).val());
	$("input[name=new-job-end]").val(form.find($("input.form-job--new-data-end")).val());
	$("input[name=new-job-target]").val(form.find($("input.form-job--new-target")).val());
	console.log('send2');
	$("#new-job").submit();
});

//редактирование информации о сотруднике
$(".change-worker--save").on("click", function(){
	form = $(this).parents(".modal");
	$("input[name=edit-worker-name]").val(form.find($(".view-info__val--name")).val());
	$("input[name=edit-worker-gender]").val(form.find($(".view-info__val--gender")).val());
	$("input[name=edit-worker-bd]").val(form.find($(".view-info__val--date")).val());
	$("input[name=edit-worker-target]").val(form.find($(".view-info__val--target")).val());
	console.log('send2');
	$("#edit-worker").submit();
});

// удаление сотрудника
$(".worker__btn--delete").on("click", function(){
	$("input[name=delete-target]").val($(this).attr("data-worker"));
});

console.log($("#edit-worker").attr('action'));
